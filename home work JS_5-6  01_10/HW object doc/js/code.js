/* Створити об'єкт "Документ", де визначити властивості "Заголовок, тіло, футер, дата". Створити об'єкт вкладений об'єкт - "Додаток". Створити об'єкт "Додаток", вкладені об'єкти, "Заголовок, тіло, футер, дата". Створити методи для заповнення та відображення документа. */

var document = {
  title: "Заголовок",
  body: "Контент",
  footer: "Футер",
  data: "Дата",
  app: {
    title: { meaning: "title" },
    body: { meaning: "body" },
    footer: { meaning: "footer" },
    data: { meaning: "data" },
  },

  newFunction: function () {
    for (let key in document) {
      if (typeof (document[key]) == "object") {
        for (let i in document[key]) {
          console.log(`Властивість ${i} значення ${document[key][i]}`);
        }
      }
      else {
        console.log(`Властивість ${key} значення ${document[key]}`)
      }
    }
  }
}

document.newFunction();










