/*
1. Створіть клас Phone, який містить змінні number, model і weight.
Створіть три екземпляри цього класу.
Виведіть на консоль значення їх змінних.
Додати в клас Phone методи: receiveCall, має один параметр - ім'я. Виводить на консоль повідомлення "Телефонує {name}". Метод getNumber повертає номер телефону. Викликати ці методи кожного з об'єктів.
*/

class Phone {
    constructor(number, model, weight) {
        this.number = number;
        this.model = model;
        this.weight = weight;
    }
    receiveCall(name) {
        console.log('Телефонує: ', name)
    }
    getNumber(number) {
        console.log(this.number)
    }
}

let Phone1 = new Phone(12346789, 'Samsung', 150);
let Phone2 = new Phone(987654321, 'Huawei', 170);
let Phone3 = new Phone(147852369, 'Xiaomi', 160);

console.dir(Phone1);
console.dir(Phone2);
console.dir(Phone3);

Phone1.receiveCall('Olena');
Phone1.receiveCall('Anton');
Phone1.receiveCall('Andrij');

Phone1.getNumber();
Phone2.getNumber();
Phone3.getNumber();

/*
2. Написати функцію filterBy(), яка прийматиме 2 аргументи. Перший аргумент - масив, який міститиме будь-які дані, другий аргумент - тип даних.
Функція повинна повернути новий масив, який міститиме всі дані, які були передані в аргумент, за винятком тих, тип яких був переданий другим аргументом. Тобто якщо передати масив c, і другим аргументом передати 'string', то функція поверне масив [23, null].
 */

function filteredBy(arr, type) {
    let newArr = [];
    for (let i = 0; i < arr.length; i++) {
        if((typeof arr[i]) !== type){
            newArr.push(arr[i])
    }
    return newArr;
  }
}
  console.dir(filteredBy(['hello', 'world', 23, '23', null], 'string'))